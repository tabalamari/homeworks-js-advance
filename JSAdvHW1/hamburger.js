/**
 * Класс, объекты которого описывают параметры гамбургера.
 *
 * @constructor
 * @param size        Размер
 * @param stuffing    Начинка
 * @throws {HamburgerException}  При неправильном использовании
 */
function Hamburger(size, stuffing) {
    if (size !== Hamburger.SIZE_SMALL && size !== Hamburger.SIZE_LARGE)
        throw new HamburgerException("wrong size received");
    if (stuffing !== Hamburger.STUFFING_CHEESE && stuffing !== Hamburger.STUFFING_SALAD && stuffing !== Hamburger.STUFFING_POTATO)
        throw new HamburgerException("wrong stuffing received");
    this.size = size;
    this.stuffing = stuffing;
    this.topping = [];

}


/* Размеры, виды начинок и добавок */
Hamburger.SIZE_SMALL = {name: "small", price: 50, calories: 20};
Hamburger.SIZE_LARGE = {name: "large", price: 100, calories: 40};
Hamburger.STUFFING_CHEESE = {name: "cheese", price: 10, calories: 20};
Hamburger.STUFFING_SALAD = {name: "salad", price: 20, calories: 5};
Hamburger.STUFFING_POTATO = {name: "potato", price: 15, calories: 10};
Hamburger.TOPPING_MAYO = {name: "mayo", price: 20, calories: 5};
Hamburger.TOPPING_SPICE = {name: "spice", price: 15, calories: 0};

/**
 * Добавить добавку к гамбургеру. Можно добавить несколько
 * добавок, при условии, что они разные.
 *
 * @param topping     Тип добавки
 * @throws {HamburgerException}  При неправильном использовании
 */

Hamburger.prototype.addTopping = function (topping) {

    if (topping !== Hamburger.TOPPING_MAYO && topping !== Hamburger.TOPPING_SPICE)
        throw new HamburgerException("wrong topping received");
    var pos = this.topping.indexOf(topping);
    if (pos !== -1)
        throw new HamburgerException ("duplicate topping");
    this.topping.push(topping);
};


/**
 * Убрать добавку, при условии, что она ранее была
 * добавлена.
 *
 * @param topping   Тип добавки
 * @throws {HamburgerException}  При неправильном использовании
 */
Hamburger.prototype.removeTopping = function (topping) {

    if (topping !== Hamburger.TOPPING_MAYO && topping !== Hamburger.TOPPING_SPICE)
        throw new HamburgerException("wrong topping received");

    var pos = this.topping.indexOf(topping);
    if (pos === -1)
        throw new HamburgerException ("this topping was not added");
    this.topping.splice(pos, 1)
};

/**
 * Получить список добавок.
 *
 * @return {Array} Массив добавленных добавок, содержит константы
 *                 Hamburger.TOPPING_*
 */
Hamburger.prototype.getToppings = function () {
    return this.topping;
};


/**
 * Узнать размер гамбургера
 */
Hamburger.prototype.getSize = function () {
    return this.size.name;
};

/**
 * Узнать начинку гамбургера
 */
Hamburger.prototype.getStuffing = function () {
    return this.stuffing;
};

/**
 * Узнать цену гамбургера
 * @return {Number} Цена в тугриках
 */
Hamburger.prototype.calculatePrice = function () {
    // - Некая сеть фастфудов предлагает два вида гамбургеров:
    //     - маленький (50 гривен, 20 калорий)
    // - большой (100 гривен, 40 калорий)
    // - Гамбургер должен включать одну дополнительную начинку (обязательно):
    // - сыр (+ 10 гривен, + 20 калорий)
    // - салат (+ 20 гривен, + 5 калорий)
    // - картофель (+ 15 гривен, + 10 калорий)
    // - Дополнительно, в гамбургер можно добавить приправу (+ 15 гривен, 0 калорий) и полить майонезом (+ 20 гривен, + 5 калорий).
    var price = 0;
    price += this.size.price;
    price += this.stuffing.price;
    this.topping.forEach (val => price += val.price);

    return price;
};

// /**
//  * Узнать калорийность
//  * @return {Number} Калорийность в калориях
//  */
Hamburger.prototype.calculateCalories = function () {
    var calories = 0;
    calories += this.size.calories;
    calories += this.stuffing.calories;
    this.topping.forEach (val => calories += val.calories);

    return calories;
};
// /**
//  * Представляет информацию об ошибке в ходе работы с гамбургером.
//  * Подробности хранятся в свойстве message.
//  * @constructor
//  */
function HamburgerException(message) {
    this.name = "HamburgerException";
    this.message = message;
}


///////////////////////////////////////////////////////////////////////////////////

function rightWay() {
// маленький гамбургер с начинкой из сыра
    try {
        var hamburger = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE);
    } catch (e) {
        console.error(e.name, e.message);
    }
// добавка из майонеза
    hamburger.addTopping(Hamburger.TOPPING_MAYO);
// сколько стоит
    console.log("Price: %f", hamburger.calculatePrice());
    // я тут передумал и решил добавить еще приправу
    hamburger.addTopping(Hamburger.TOPPING_SPICE);
    // спросим сколько там калорий
    console.log("Calories: %f", hamburger.calculateCalories());

// А сколько теперь стоит?
    console.log("Price with sauce: %f", hamburger.calculatePrice());
// Проверить, большой ли гамбургер?
    console.log("Is hamburger large: %s", hamburger.getSize() === Hamburger.SIZE_LARGE); // -> false
// Убрать добавку
    hamburger.removeTopping(Hamburger.TOPPING_SPICE);
    console.log("Have %d toppings", hamburger.getToppings().length); // 1
}

/////////////////////////////////////////////////////////////////////////////////////////////

function wrongWay() {
// не передали обязательные параметры
//     var h2 = new Hamburger(); // => HamburgerException: no size given

// передаем некорректные значения, добавку вместо размера
//     var h3 = new Hamburger(Hamburger.TOPPING_SPICE, Hamburger.TOPPING_SPICE);
// => HamburgerException: invalid size 'TOPPING_SAUCE'

// добавляем много добавок
    var h4 = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE);
    h4.addTopping(Hamburger.TOPPING_MAYO);
    h4.addTopping(Hamburger.TOPPING_MAYO);
// HamburgerException: duplicate topping 'TOPPING_MAYO'
}

rightWay();
// wrongWay();